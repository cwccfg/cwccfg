#!/usr/bin/env python3

# This file is part of CWCCFG.
#
# CWCCFG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CWCCFG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CWCCFG.  If not, see <http://www.gnu.org/licenses/>.

import os
import stat
import sys

SHEBANG = """\
#!/usr/bin/env {}

"""

HEADER = """\
# This file is part of CWCCFG.
#
# CWCCFG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CWCCFG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CWCCFG.  If not, see <http://www.gnu.org/licenses/>.

"""

INTERPRETERS = {
    'py': 'python3',
    'sh': 'bash'
  # 'sls': None,  # noqa
  # 'yml': None   # noqa
}


def check_header(filename: str) -> bool:

    try:
        exec_bit = stat.S_IXUSR
        executable = exec_bit == exec_bit & os.stat(filename)[stat.ST_MODE]
    except Exception as e:
        sys.stderr.write("ERROR for file {}: {}\n".format(filename, str(e)))
        return 1

    expected_header = HEADER
    if executable:
        extension = filename.rpartition('.')[2]
        expected_header = SHEBANG.format(INTERPRETERS[extension]) + HEADER

    try:
        with open(filename, 'r', encoding='utf-8') as file_to_check:
            header = file_to_check.read(len(expected_header))
    except Exception as e:
        sys.stderr.write("ERROR for file {}: {}\n".format(filename, str(e)))
        return 1

    if header != expected_header:
        sys.stderr.write("ERROR for file {}: header did not match\n"
                         .format(filename))
        return 1

    return 0


def main(argv) -> int:
    if len(argv) != 2:
        sys.stderr.write("Usage: {} filename\n".format(argv[0]))
        return 1

    return check_header(argv[1])


if __name__ == '__main__':
    sys.exit(main(sys.argv))
