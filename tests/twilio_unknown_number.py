#!/usr/bin/env python3

# This file is part of CWCCFG.
#
# CWCCFG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CWCCFG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CWCCFG.  If not, see <http://www.gnu.org/licenses/>.

import os
import subprocess
import sys
import time

import twilio.twiml as twiml

url = 'localhost:8080/incoming_sms'
params = ('ToCountry=US&ToState=MA&SmsMessageSid=SM0&NumMedia=0&ToCity='
          '&FromZip=00000&SmsSid=SM0&FromState=MA&SmsStatus=received&Fr'
          'omCity=BOSTON&Body=TEST&FromCountry=US&To=%2B15555555555&ToZ'
          'ip=&NumSegments=1&MessageSid=SM0&AccountSid=AC0&From=%2B1555'
          '5555555&ApiVersion=2010-04-01')


def test_sms_unknown_client(config: str) -> int:
    env = os.environ.copy()
    env['CWCCFG_CONFIG'] = config
    subp = subprocess.Popen(['python3', 'src/app.py'], env=env)
    # Wait 5 seconds for it the server to come up
    time.sleep(5)

    p = subprocess.Popen(['curl', url + '?' + params],
                         stdout=subprocess.PIPE,
                         stderr=subprocess.DEVNULL)
    output, _ = p.communicate()
    p.wait()

    expected_output = twiml.Response()
    expected_output.message(("Hello! Unfortunately, we don't know you. If "
                             "you're a CWC client, please give them a call."))

    if output.decode() != str(expected_output):
        subp.kill()
        return 1

    subp.kill()
    return 0


def main(argv) -> int:
    if len(argv) != 2:
        sys.stderr.write('Usage: {} config\n'.format(argv[0]))
        return 1

    return test_sms_unknown_client(argv[1])

if __name__ == '__main__':
    sys.exit(main(sys.argv))
