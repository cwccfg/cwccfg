# This file is part of CWCCFG.
#
# CWCCFG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CWCCFG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CWCCFG.  If not, see <http://www.gnu.org/licenses/>.

from collections import namedtuple
from datetime import datetime, timedelta


class Appointment(namedtuple('Appointment',
                             ['coach', 'client', 'time', 'duration'])):

    def __new__(cls, coach: str, client: str,
                time: datetime, duration: timedelta):
        """
        Create an appointment.

        The coach argument is the name of the coach.
        The client argument is the name of the client.
        The time argument is the time of the appointment in 12hr time.
        The duration argument is the length of the appointment in minutes.
        """
        return super(Appointment, cls).__new__(cls, coach, client,
                                               time, duration)

    def __repr__(self) -> str:
        return "Appointment({}, {}, {}, {})".format(self.coach,
                                                    self.client,
                                                    self.time,
                                                    self.duration)
