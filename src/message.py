# This file is part of CWCCFG.
#
# CWCCFG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CWCCFG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CWCCFG.  If not, see <http://www.gnu.org/licenses/>.

import collections


class Message(collections.namedtuple('Message', [
                                         'id',
                                         'sender',
                                         'recipient',
                                         'message'
                                    ])):

    def __new__(cls, id: str, sender: str, recipient: str, message: str):
        """
        Create a message.

        The id argument must be any unique identifier.
        The sender argument must be the phone number that sent the message.
        The recipient argument must be the phone number that received the
            message.
        The message argument must be the body of the text.
        """
        return super(Message, cls).__new__(cls, id, sender, recipient, message)

    def __repr__(self) -> str:
        return 'Message({}, {}, {}, {})'.format(
            self.id, self.sender, self.recipient, self.message)
