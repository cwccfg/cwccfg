#!/usr/bin/env python3

# This file is part of CWCCFG.
#
# CWCCFG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CWCCFG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CWCCFG.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime, timedelta, timezone
import logging
import sys

from babel.dates import format_timedelta
import i18n
import yaml

import app
from appointment import Appointment
from calendars import CompositeCalendar, ICSCalendar
from database import Client, Coach, Database
from sms import TwilioSMSGateway
from translation import with_lang


logger = logging.getLogger(app.APP_NAME)


def make_calendar(name, url):
    try:
        return ICSCalendar(name, url)
    except Exception as e:
        logger.error(str(e))
        print(('Coach {} has an invalid calendar URL! His clients '
               'will not be notified').format(name))


def load_appointments(db, earliest, latest):
    with db.transaction() as trans:
        cals = filter(lambda x: x is not None,
                      map(lambda coach: make_calendar(coach.name,
                                                      coach.ics_url),
                          trans.get_coaches()))
        composite = CompositeCalendar(cals)
        return composite.get_appts(earliest, latest)


def build_reminder(client: Client, coach: Coach,
                   appointment: Appointment) -> str:
    """
    Build the reminder string that is sent to the client via SMS.
    Contains the coach and time of the appointment.
    """
    apt_time = appointment.time.astimezone(app.Context.DEFAULT_TIMEZONE)
    lang = client.language
    return with_lang(lang, 'appt_reminder').format(
            coach.name,
            format_timedelta(apt_time - datetime.now(timezone.utc),
                             locale=lang),
            apt_time.strftime('%I:%M %p %Z'),
            format_timedelta(appointment.duration,
                             locale=lang))


def main() -> int:
    """
    Sends reminders for all appointments scheduled tomorrow.
    Meant to be run periodically (e.g. as a cron job).
    """
    try:
        with open('config.yml', 'r') as conf:
            config = yaml.safe_load(conf)
    except Exception as e:
        sys.stderr.write("ERROR: {}\n".format(str(e)))
        return 1

    i18n.load_path.append(config[app.Context.CONF_KEY_TRANSLATION_PATH])
    i18n.set('fallback', app.Context.FALLBACK_LOCALE)

    logging.basicConfig(format='%(asctime)s %(message)s')

    missed_reminders = 0

    twilio_config = config['twilio']
    gateway = TwilioSMSGateway(twilio_config['account_id'],
                               twilio_config['token'],
                               twilio_config['outgoing_number'])
    db = Database(config)
    appointments = load_appointments(db,
                                     datetime.now(timezone.utc),
                                     datetime.now(timezone.utc) +
                                     timedelta(hours=24))

    with db.transaction() as transaction:
        for appointment in appointments:
            client = transaction.get_clients(name=appointment.client).first()
            if client is None:
                logger.warn('Could not find client with name {}'.format(
                    appointment.client))
                missed_reminders += 1
                continue

            coach = transaction.get_coaches(name=appointment.coach).first()
            if coach is None:
                logger.warn('Could not find coach with name {}'.format(
                    appointment.coach))
                missed_reminders += 1
                continue

            reminder = build_reminder(client, coach, appointment)

            try:
                gateway.send_sms(client.phone_number, reminder)
            except Exception as e:
                logger.warn(e)
                missed_reminders += 1

    if missed_reminders > 0:
        logger.error('{} reminders failed to send!'.format(
            missed_reminders))

    return 0


if __name__ == '__main__':
    sys.exit(main())
