# This file is part of CWCCFG.
#
# CWCCFG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CWCCFG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CWCCFG.  If not, see <http://www.gnu.org/licenses/>.

import i18n


def t(x: str) -> str:
    """Translate a string to the current locale."""
    return i18n.t('app.' + x)


def set_lang(lang: str):
    """Set the current locale. The argument should be a 2-letter string."""
    i18n.set('locale', lang)


def with_lang(lang: str, x: str) -> str:
    """
    Gets a translation with the specified language, but doesn't modify
    the global language setting.
    """
    old = i18n.get('locale')
    i18n.set('locale', lang)
    result = i18n.t('app.' + x)
    if old != lang:
        i18n.set('locale', lang)
    return result
