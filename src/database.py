# This file is part of CWCCFG.
#
# CWCCFG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CWCCFG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CWCCFG.  If not, see <http://www.gnu.org/licenses/>.

from sqlalchemy import Column, create_engine, ForeignKey, \
        Integer, Sequence, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker

Base = declarative_base()
Session = sessionmaker()


class Client(Base):
    """A client (name-phone-language triple)."""
    __tablename__ = 'clients'

    # Possible client states
    # extra: none
    STATE_CLEAN = 0

    # extra: time of appointment being rescheduled
    STATE_RESCHEDULE = 1

    # extra: time of appointment being rescheduled $$ new time for appointment
    STATE_WAITING_COACH_CONFIRM = 2

    # extra: time of appointment being rescheduled $$ new time for appointment
    STATE_WAITING_CLIENT_CONFIRM = 3

    id = Column(Integer, Sequence('client_id_seq'), primary_key=True)
    name = Column(String)
    phone_number = Column(String, unique=True)
    language = Column(String)
    coach_id = Column(Integer, ForeignKey('coaches.id'))

    reschedule_state = Column(Integer)
    reschedule_extra = Column(String)

    def __repr__(self):
        return ("<Client(name='{}', phone_number='{}', language='{}', "
                "state='{}', extra='{}')>").format(self.name,
                                                   self.phone_number,
                                                   self.language,
                                                   self.reschedule_state,
                                                   self.reschedule_extra)


class Coach(Base):
    """A coach (name)."""
    __tablename__ = 'coaches'

    id = Column(Integer, Sequence('coach_id_seq'), primary_key=True)
    name = Column(String, unique=True)  # probably not unique
    ics_url = Column(String, nullable=True)
    email = Column(String, unique=True)
    clients = relationship('Client')

    def __repr__(self):
        return "<Coach(name='{}', email='{}')>".format(self.name, self.email)


class _Transaction(object):
    class Cancel(Exception):
        """Used to break out of transactions without committing."""

    def __init__(self, owner):
        self._owner = owner

    def __enter__(self):
        self._session = Session()
        return self

    def __exit__(self, etype, evalue, traceback):
        if etype == self.Cancel:
            self._session.rollback()
            self._session.close()
            return True  # This consumes the exception
        elif etype is None:
            self._session.commit()
            self._session.close()
        else:
            self._session.rollback()
            self._session.close()

    def new_coach(self, name, email, ics_url=None):
        """Adds a new coach to the database."""
        self._session.add(Coach(name=name, email=email,
                                clients=[], ics_url=ics_url))

    def get_coaches(self, name: str = None):
        """
        Finds coaches with the given name in the database.

        Does not return a list, but an iterable.
        """
        query = self._session.query(Coach)
        if name is None:
            return query
        return query.filter_by(name=name)

    def new_client(self, name, phone, lang, coach_id):
        """Adds a new client to the database."""
        self._session.add(Client(name=name, phone_number=phone,
                                 language=lang, coach_id=coach_id,
                                 reschedule_state=Client.STATE_CLEAN,
                                 reschedule_extra=''))

    def get_client_coach(self,
                         client=None,
                         client_name: str=None,
                         phone_number: str=None):
        """
        Finds the coach of a client in the database.
        """
        query = self._session.query(Coach)
        if client is not None:
            return query.get(client.coach_id)
        else:
            clients = self.get_clients(name=client_name,
                                       phone_number=phone_number)
            if len(clients) == 0:
                return None
            return self.get_client_coach(client=clients[0])

    def get_clients(self, name: str = None, phone_number: str = None):
        """
        Finds clients from the database. At least one of the arguments "name"
        and "number" must be specified, or all clients will be found.

        Does not return a list, but an iterable.
        """
        query = self._session.query(Client)
        if name is None and phone_number is None:
            return query
        elif name is None:
            return query.filter_by(phone_number=phone_number)
        elif phone_number is None:
            return query.filter_by(name=name)
        else:
            return query.filter_by(phone_number=phone_number, name=name)

    def delete_client(self, name: str, phone_number: str) -> bool:
        """
        Deletes all matching clients from the database. Returns true
        if a change to the DB was made.
        """
        query = self._session.query(Client)
        deleted = False
        for client in query.filter_by(name=name,
                                      phone_number=phone_number):
            self._session.delete(client)
            deleted = True
        return deleted

    def delete(self, obj):
        """
        Deletes the given object from the database.
        """
        self._session.delete(obj)


class Database(object):

    def __init__(self, config: dict):
        """
        Creates a database connection. This will lazily create the database
        if needed.

        The app config should have a 'db_path' element, which contains the
        relative (or absolute, if it begins with a forward slash) path to the
        database file.
        """
        self._engine = create_engine('sqlite:///' + config['db_path'])

        Session.configure(bind=self._engine)
        Base.metadata.create_all(self._engine)

    def transaction(self):
        """
        Creates a transaction instance for this connection.
        Only one transaction may be active at a time.

        To use the database in a transactional way, do:
        with database.transaction() as transaction:
            transaction.new_client(client)

        As you leave the with block, the transaction is committed.
        If you want to abort the transaction, exit the with block as follows:
        with database.transaction() as transaction:
            transaction.new_client(client)
            # Some logic here... uh oh, better abort!
            raise transaction.Cancel

        If this happens, the transaction will not be committed and all
        changes will be lost.
        """
        return _Transaction(self)
