# This file is part of CWCCFG.
#
# CWCCFG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CWCCFG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CWCCFG.  If not, see <http://www.gnu.org/licenses/>.


import abc
from datetime import datetime, timedelta
import hashlib
import re
from urllib.request import urlopen

from babel.dates import get_timezone
from ics import Calendar as ICalendar
from pyexchange import Exchange2010Service, ExchangeNTLMAuthConnection

from database import Client, Coach
from appointment import Appointment


class Calendar(object):
    """
    Represents the calendar of a coach.
    """
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def next_appointment(self, coach: Coach, client: Client=None):
        """
        Get the next appointment (in the next 3 months) with the given client,
        or else None.
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def get_events(self, start, end):
        """
        Should return a list of all events on this calendar, not just client
        appointments, starting at start and ending at end.
        Start and end will be tz-aware datetimes in UTC.
        The returned events should have 'start' and 'end' attributes that are
        tz-aware datetimes in UTC as well.
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def get_appts(self, start, end):
        """
        Should return a list of all client appointments on this calendar
        starting at start and ending at end.
        Start and end will be tz-aware datetimes in UTC.
        """
        raise NotImplementedError()

    def get_possible_appts(self,
                           near_time,
                           appt_length,
                           notbefore=None,
                           bound=timedelta(days=7)):
        """
        Return a list of up to 3 datetimes.

        near_time should be a tz-aware datetime in UTC.
        appt_length should be a timedelta.
        notbefore should beNone, or a tz-aware datetime in UTC before which
        appointments should not be returned
        bound should be a timedelta which specifies how far from near_time
        we should look around
        """
        start = near_time - bound
        end = near_time + bound
        if notbefore:
            if end < notbefore:
                return []
            if start < notbefore <= end:
                start = notbefore

        events = sorted(self.get_events(start=start,
                                        end=end),
                        key=lambda e: e.start)

        slots = []
        slot = (start + timedelta(hours=2)).replace(
                minute=0, second=0, microsecond=0)

        # For now, assume EST for coaches
        local_timezone = get_timezone('US/Eastern')

        while slot + appt_length < end:
            localized_slot = slot.astimezone(local_timezone)
            if (localized_slot.isoweekday() <= 5 and
                localized_slot.hour > 9 and
               6 < (localized_slot + appt_length).hour < 17):
                slots.append(slot)
            slot = slot + timedelta(minutes=30)

        cushion = timedelta(minutes=30)

        open_slots = []
        for slot in slots:
            # This could definitely be more efficient
            has_overlaps = False
            for event in events:
                full_slot_start = slot - cushion
                full_slot_end = slot + appt_length + cushion

                if ((full_slot_start <= event.start < full_slot_end) or
                    (full_slot_start < event.end <= full_slot_end) or
                    (event.start <= full_slot_start and
                     full_slot_end <= event.end)):
                    has_overlaps = True

            if not has_overlaps:
                open_slots.append(slot)

        def shuffle_key(slot):
            """
            Uses hashes of the slot datetime to form a randomized,
            deterministic key for sorted.
            """
            m = hashlib.sha512()
            s = slot.strftime("%A, %b %d, %Y at %I:%M %P")
            m.update(s.encode('utf-8'))
            return m.digest()

        rand_slots = sorted(open_slots, key=lambda slot: shuffle_key(slot))
        return sorted(rand_slots[:5])


class MockCalendar(Calendar):
    """
    Mock calendar useful for testing.
    """

    def __init__(self, config):
        pass

    def next_appointment(self, coach: Coach, client: Client=None):
        return None


class OutlookCalendar(Calendar):
    """
    Outlook Calendar hosted in an on-premises Exchange instance.
    """

    def __init__(self, config):
        self.coach_name = config['coach_name']
        # TODO: error handling of malformed config
        connection = ExchangeNTLMAuthConnection(**config)
        service = Exchange2010Service(connection)
        self._calendar = service.calendar()

    def next_appointment(self, coach: Coach, client: Client = None):
        def is_with_client(appt: Appointment, client: Client) -> bool:
            if client is None:
                return True
            for attendee in appt.attendees:
                if client.name == attendee.name:
                    return True
            return False

        now = datetime.utcnow()
        # TODO: make the timedelta length configurable
        end = now + timedelta('92')  # ceil(365/4) for ~3 months

        appts = self._calendar.list_events(
            # TODO Do these need to be localized for a timezone?
            start=now,
            end=end,
            details=True
        )
        client_appts = [appt for appt in appts if is_with_client(appt, client)]
        # pyexchange docs are unclear if returned events are sorted
        sorted_appts = sorted(client_appts, key=lambda appt: appt.start)

        if len(sorted_appts) <= 0:
            return None
        appt = sorted_appts[0]
        return Appointment(self.coach_name, client.name, appt.start,
                           appt.end - appt.start)

    def get_events(self, start, end):
        return self._calendar.list_events(
            start=start,
            end=end,
            details=True
        )


class ICSCalendar(Calendar):
    """
    ICS-file based Calendar
    """
    MEETING_KEY = 'client meeting with'

    def __init__(self, name, url):
        req = urlopen(url)
        cal = ICalendar(req.read().decode('utf-8'))
        self.coach_name = name
        self.events = sorted(cal.events, key=lambda event: event.begin)
        self.appt_events = sorted(list(filter(lambda event: event.name.lower()
                                              .startswith(ICSCalendar.
                                                          MEETING_KEY),
                                              cal.events)),
                                  key=lambda event: event.begin)

    @staticmethod
    def _get_client_name(evt):
        # (?i) makes the regex case-insensitive, allowing for case-insensitive
        # replacement.
        return re.sub(r'(?i)' + ICSCalendar.MEETING_KEY, '', evt.name).strip()

    def get_events(self, start, end):
        events = []
        for event in self.events:
            if start < event.begin < end:
                event.start = event.begin.datetime
                event.end = event.start + event.duration
                events.append(event)
        return events

    def get_appts(self, start, end):
        appt_events = []
        for event in self.appt_events:
            if start < event.begin < end:
                appt_events.append(event)

        appts = []
        for appt_event in appt_events:
            name = ICSCalendar._get_client_name(appt_event)
            appt = Appointment(self.coach_name,
                               name,
                               appt_event.begin.datetime,
                               appt_event.duration)
            appts.append(appt)
        return appts

    def next_appointment(self, coach: Coach, client: Client = None):
        coach_name = coach.name if coach is not None else self.coach_name
        if client is None:
            # If we don't filter by client, just return the first event
            return Appointment(coach_name,
                               ICSCalendar._get_client_name(
                                   self.appt_events[0]),
                               self.appt_events[0].begin.datetime,
                               self.appt_events[0].duration)
        for event in self.appt_events:
            name = ICSCalendar._get_client_name(event)
            if name == client.name:
                return Appointment(coach_name,
                                   name,
                                   event.begin.datetime,
                                   event.duration)
        return None


class CompositeCalendar():
    """
    Interface to query multiple Calendars at once, since clients may
    occasionally work with a different coach than their own.
    """

    def __init__(self, calendars):
        self.calendars = calendars

    def next_appointment(self, client: Client = None):
        appts = [calendar.next_appointment(client)
                 for calendar in self.calendars]
        sorted_appts = sorted(appts,
                              key=lambda appt: appt.time
                              if appt is not None else None)
        return sorted_appts[0] if len(sorted_appts) > 0 else None

    def get_events(self, start, end):
        events = []
        for cal in self.calendars:
            cal_events = cal.get_events(start, end)
            events += cal_events
        return sorted(events, key=lambda event: event.start)

    def get_appts(self, start, end):
        appts = []
        for cal in self.calendars:
            cal_appts = cal.get_appts(start, end)
            appts += cal_appts

        return sorted(appts, key=lambda appt: appt.time)
