#!/usr/bin/env python3

# This file is part of CWCCFG.
#
# CWCCFG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CWCCFG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CWCCFG.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime, timezone
from functools import partial
import logging
import os
import re
import traceback

from babel.dates import get_timezone, format_date, format_time
from calendars import ICSCalendar
from flask import Flask, request, render_template, redirect, url_for
import i18n
import iso8601
from sqlalchemy.exc import IntegrityError
import yaml

from database import Client, Database
import emails
from sms import TwilioSMSGateway
from translation import with_lang

APP_NAME = 'CWCCFG'
CONFIG_ENV = APP_NAME + '_CONFIG'
DEBUG_ENV = APP_NAME + '_DEBUG'


class Context:
    """The main contextual information for CWCCFG."""
    CONF_KEY_BIND_ADDRESS = 'bind_address'
    CONF_KEY_LANGUAGES = 'languages'
    CONF_KEY_PORTAL_PATH = 'portal_path'
    CONF_KEY_OUTPUT_LEVEL = 'output_level'
    CONF_KEY_TRANSLATION_PATH = 'translation_path'
    CONF_KEY_TWILIO_ACCOUNT_ID = 'account_id'
    CONF_KEY_TWILIO_TOKEN = 'token'
    CONF_KEY_TWILIO_SECRET = 'twilio_secret'
    CONF_KEY_TWILIO_OUTGOING = 'outgoing_number'
    DEFAULT_TIMEZONE = get_timezone('US/Eastern')
    FALLBACK_LOCALE = 'en'
    DATETIME_SEPARATOR = ' $$ '

    def __init__(self, config_path):
        with open(config_path, 'r') as conf:
            self.config = yaml.safe_load(conf)

        self.debug = os.environ.get(DEBUG_ENV, '') == 'true'

        i18n.load_path.append(self.config[Context.CONF_KEY_TRANSLATION_PATH])
        i18n.set('fallback', Context.FALLBACK_LOCALE)

        self.database = Database(self.config)

        self.SMSGatewayClass = None

        # Check which provider we use
        if 'twilio' in self.config:
            self.SMSGatewayClass = TwilioSMSGateway

        self.sms_gateway = self.SMSGatewayClass(**self.config['twilio'])

        self.email_gateway = emails.PROVIDERS[
            self.config['email']['provider']
        ](self.config['email']['config'])

        # Set the logging level.
        logging.basicConfig(format='%(asctime)s %(message)s')
        output_level = self.config.get(Context.CONF_KEY_OUTPUT_LEVEL, 'NOTSET')
        self.logger = logging.getLogger('werkzeug')
        self.logger.setLevel(output_level)

        self.app_url = self.config['app_url']

        # Add the portal route
        app.add_url_rule('/' + self.config[Context.CONF_KEY_PORTAL_PATH],
                         'portal_handler', portal_handler,
                         methods=['GET', 'POST'])
        app.add_url_rule('/' + self.config[Context.CONF_KEY_PORTAL_PATH] +
                         '/clients/<int:client_id>',
                         'client_info', client_info,
                         methods=['GET', 'POST'])
        app.add_url_rule('/' + self.config[Context.CONF_KEY_TWILIO_SECRET],
                         'incoming_sms', incoming_sms, methods=['GET'])


app = Flask(APP_NAME, template_folder='./templates')
context = None


def incoming_sms():
    """Handles an incoming SMS message, and tells twilio how to respond."""
    message = context.SMSGatewayClass.parse_message(request.args)
    logger = context.logger

    with context.database.transaction() as trans:
        clients = list(trans.get_clients(phone_number=message.sender))
        if len(clients) == 0:
            return context.SMSGatewayClass.build_response(
                handle_unknown_client(message.sender, context.logger))

        client = clients[0]
        state = client.reschedule_state
        coach = trans.get_client_coach(client=client)

        t = partial(with_lang, client.language)

        # Wrapper around build_response that returns an unknown message
        # response if the generated response is None. This simplifies message
        # handlers and reduces code duplication.

        def r(m):
            if m is not None:
                return context.SMSGatewayClass.build_response(m)
            else:
                return context.SMSGatewayClass.build_response(
                        t('unknown_message'))
        message = message.message.lower().strip()

        # Handle state-independent commands
        if message == t('next_appt'):
            return r(handle_next_appt(client, coach, logger, t))
        elif message == t('help'):
            return r(handle_help(client, coach, logger, t))

        # Dispatch handling based on client state
        if state == Client.STATE_CLEAN:
            return r(handle_message_clean(message, client, coach, logger, t))
        elif state == Client.STATE_RESCHEDULE:
            return r(handle_message_reschedule(message, client, coach,
                                               logger, t))
        elif state == Client.STATE_WAITING_CLIENT_CONFIRM:
            return r(handle_message_waiting_client_confirm(message, client,
                                                           coach, logger, t))
        elif state == Client.STATE_WAITING_COACH_CONFIRM:
            return r(handle_message_waiting_coach_confirm(message, client,
                                                          coach, logger, t))
        else:
            raise Exception('client {} in invalid reschedule state {}'
                            .format(client.name, client.reschedule_state))


@app.errorhandler(Exception)
def handle_error(err):
    context.logger.error(str(err))
    context.logger.error(traceback.format_exc())
    return context.SMSGatewayClass.build_response(
            with_lang(Context.FALLBACK_LOCALE, 'internal_error'))


def handle_message_clean(message, client, coach, logger, t):
    if message == t('reschedule'):
        logger.info('received a reschedule request from {}'
                    .format(client.name))

        calendar = ICSCalendar(coach.name, coach.ics_url)
        next_appt = calendar.next_appointment(coach, client=client)
        if next_appt is None:
            return t('no_appts')

        client.reschedule_state = Client.STATE_RESCHEDULE
        client.reschedule_extra = str(next_appt.time)

        possible = calendar.get_possible_appts(next_appt.time,
                                               next_appt.duration,
                                               notbefore=datetime.now(timezone.utc))
        return _get_possible_appt_message(coach, client, possible, t)

    return None


def handle_message_reschedule(message, client, coach, logger, t):
    if message == t('cancel'):
        client.reschedule_state = Client.STATE_CLEAN
        client.reschedule_extra = ''
        return t('reschedule_cancel')
    elif message == t('choices'):
        calendar = ICSCalendar(coach.name, coach.ics_url)
        next_appt = calendar.next_appointment(coach, client=client)
        if next_appt is None:
            client.reschedule_state = Client.STATE_CLEAN
            client.reschedule_extra = ''
            return t('no_appts')
        possible = calendar.get_possible_appts(next_appt.time,
                                               next_appt.duration,
                                               notbefore=datetime.now(timezone.utc))
        return _get_possible_appt_message(coach, client, possible, t)

    try:
        selection = int(message)
    except ValueError:
        logger.debug('could not parse client selection {}'.format(message))
        return None

    serialized_time = iso8601.parse_date(client.reschedule_extra)
    if datetime.now(timezone.utc) > serialized_time:
        # The appointment has passed
        client.reschedule_state = Client.STATE_CLEAN
        client.reschedule_extra = ''
        return t('appointment_happened')
    calendar = ICSCalendar(coach.name, coach.ics_url)
    next_appt = calendar.next_appointment(coach, client=client)
    if next_appt is None:
        client.reschedule_state = Client.STATE_CLEAN
        client.reschedule_extra = ''
        return t('no_appts')
    possible = calendar.get_possible_appts(next_appt.time,
                                           next_appt.duration,
                                           notbefore=datetime.now(timezone.utc))

    # Note: client selections are 1-indexed
    if selection > len(possible) or selection < 1:
        return t('invalid_selection')
    selection = possible[selection - 1]

    client.reschedule_state = Client.STATE_WAITING_CLIENT_CONFIRM
    client.reschedule_extra = str(serialized_time) + \
        Context.DATETIME_SEPARATOR + \
        str(selection)

    old_dt = serialized_time.astimezone(Context.DEFAULT_TIMEZONE)
    odate = format_date(old_dt, locale=client.language)
    otime = format_time(old_dt, locale=client.language, format='short')

    new_dt = selection.astimezone(Context.DEFAULT_TIMEZONE)
    ndate = format_date(new_dt, locale=client.language)
    ntime = format_time(new_dt, locale=client.language, format='short')

    return t('confirm_selection').format(coach.name,
                                         odate, otime,
                                         ndate, ntime)


def handle_message_waiting_client_confirm(message, client, coach, logger, t):
    [serialized_old, serialized_new] = map(
            iso8601.parse_date,
            client.reschedule_extra.split(Context.DATETIME_SEPARATOR))

    if datetime.now(timezone.utc) > serialized_old or \
            datetime.now(timezone.utc) > serialized_new:
        # The appointment has passed
        client.reschedule_state = Client.STATE_CLEAN
        client.reschedule_extra = ''
        return t('appointment_happened')

    local_old = serialized_old.astimezone(Context.DEFAULT_TIMEZONE)
    local_new = serialized_new.astimezone(Context.DEFAULT_TIMEZONE)
    if message == t('yes') or message == t('yes')[0]:
        email_body = ("Hello {},\n"
            "Your client {} wants to reschedule their appointment from {} "
            "to {}.\n\n"
            "To approve or reject this appointment, please visit the "
            "following url: {}\n\n"
            "----\n"
            "This is an automated message.").format(coach.name,
                    client.name,
                    local_old.strftime("%A, %b %d, %Y at %I:%M %p"),
                    local_new.strftime("%A, %b %d, %Y at %I:%M %p"),
                    context.app_url + url_for('client_info',
                                              client_id=client.id))
        email_subject = "{} Would Like to Reschedule".format(client.name)
        try:
            context.email_gateway.send_email(coach.email,
                                             email_subject,
                                             email_body)
        except emails.EmailException as e:
            logger.error(str(e))
            return t('could_not_message_coach')
        else:
            client.reschedule_state = Client.STATE_WAITING_COACH_CONFIRM
            return t('waiting_coach_confirm')
    elif message == t('no') or message == t('no')[0]:
        client.reschedule_state = Client.STATE_CLEAN
        client.reschedule_extra = ''
        return t('reschedule_cancel')

    return None


def handle_message_waiting_coach_confirm(message, client, coach, logger, t):
    if message == t('cancel'):
        client.reschedule_state = Client.STATE_CLEAN
        client.reschedule_extra = ''
        return t('reschedule_cancel')

    return t('waiting_coach_confirm')


def _get_possible_appt_message(coach, client, possible, t):
    msg = t('coach_appointment_slots').format(coach.name)
    msg += '\n'
    fmtstr = '{} ' + t('at') + ' {}'
    if len(possible) == 0:
        client.reschedule_state = Client.STATE_CLEAN
        client.reschedule_extra = ''
        return t('no_possible_appts')
    for i, option in enumerate(possible):
        msg += str(i + 1) + '. ' + ' '
        local_dt = option.astimezone(Context.DEFAULT_TIMEZONE)
        msg += fmtstr.format(format_date(local_dt,
                                         locale=client.language),
                             format_time(local_dt,
                                         locale=client.language,
                                         format='short'))
        msg += '\n'

    return msg + t('reschedule_info')


def handle_next_appt(client, coach, logger, t):
    logger.info('received a next appointment request from {}'
                .format(client.name))

    # Maybe cache this in the future? This downloads
    # the whole thing, unfortunately
    calendar = ICSCalendar(coach.name, coach.ics_url)
    appt = calendar.next_appointment(coach, client=client)
    if appt is None:
        return t('no_appts')
    local_dt = appt.time.astimezone(Context.DEFAULT_TIMEZONE)
    date = format_date(local_dt, locale=client.language)
    time = format_time(local_dt, locale=client.language, format='short')
    return t('next_appt_resp').format(coach.name, date, time)


def handle_help(client, coach, logger, t):
    logger.info('received a help request from {}'
                .format(client.name))

    state = client.reschedule_state
    if state == Client.STATE_CLEAN:
        message = t('default_help_text')
    elif state == Client.STATE_RESCHEDULE:
        message = t('reschedule_help_text')
    elif state == Client.STATE_WAITING_CLIENT_CONFIRM:
        [serialized_old, serialized_new] = map(
                iso8601.parse_date,
                client.reschedule_extra.split(Context.DATETIME_SEPARATOR))

        old_dt = serialized_old.astimezone(Context.DEFAULT_TIMEZONE)
        odate = format_date(old_dt, locale=client.language)
        otime = format_time(old_dt, locale=client.language, format='short')

        new_dt = serialized_new.astimezone(Context.DEFAULT_TIMEZONE)
        ndate = format_date(new_dt, locale=client.language)
        ntime = format_time(new_dt, locale=client.language, format='short')

        message = t('waiting_client_confirm_help_text').format(
                odate, otime, ndate, ntime)
    elif state == Client.STATE_WAITING_COACH_CONFIRM:
        message = t('waiting_coach_confirm_help_text')
    else:
        raise Exception('client {} in invalid state {}'.format(
            client.name, state))

    return '{} {}'.format(t('base_help_text'), message)


def handle_unknown_client(number, logger):
    logger.info('received a message from unknown number {}'
                .format(number))

    return with_lang('en', 'unknown_number')


def validate_phone_number(phone: str) -> bool:
    # TODO maybe use twilio to validate that it's a mobile
    # number?
    return phone.startswith('+')


def normalize_phone_number(phone: str) -> str:
    # Strip out characters like parens and dashes - in
    # fact, all non-valid characters.
    return re.sub(r'[^0-9\+x]', '', phone)


def validate_name(name: str) -> bool:
    return len(name) > 0


def portal_handler():
    if request.method == 'GET':
        languages = context.config[Context.CONF_KEY_LANGUAGES]
        languages = [{
            'id': key,
            'name': languages[key]
        } for key in sorted(languages.keys())]
        with context.database.transaction() as transaction:
            clients = transaction.get_clients()
            clients = [{
                'name': client.name,
                'phone': client.phone_number,
            } for client in clients]
            coaches = transaction.get_coaches()
            coaches = [coach.name for coach in coaches]
        return render_template('admin.jinja', **{
            'languages': languages,
            'secret': url_for('portal_handler'),
            'clients': clients,
            'coaches': coaches,
            # We whitelist the query parameter to prevent XSS
            'message': {
                'cnf': 'Coach not found!',
                'ok0': 'Client added!',
                'ok1': 'Coach added!',
                'ok2': 'Client removed!',
                'ok3': 'Coach removed!',
                'ok4': 'Client reassigned!',
                'phn': 'Phone number in use!',
                'nrm': 'Name/phone number pair not found!',
                'eph': 'Invalid phone number!',
                'chc': 'Can not delete coach with clients!',
                'cmf': 'Client not found!',
                'php': ('Could not send message to client, but they were '
                        'still added. Please double check their phone '
                        'number, remove, and re-add them.'),
                'enm': 'Name can not be empty!',
                'cdu': 'Coach already exists!',
                'eur': 'Empty URL not allowed!',
                'err': 'Database error!',
            }.get(request.args.get('m', ''), ''),
            'messageclass': {
                'cnf': 'text-warning',
                'ok0': 'text-success',
                'ok1': 'text-success',
                'ok2': 'text-success',
                'ok3': 'text-success',
                'ok4': 'text-success',
                'phn': 'text-warning',
                'nrm': 'text-warning',
                'eph': 'text-warning',
                'chc': 'text-warning',
                'cmf': 'text-warning',
                'php': 'text-warning',
                'enm': 'text-warning',
                'cdu': 'text-warning',
                'eur': 'text-warning',
                'err': 'text-danger',
            }.get(request.args.get('m', ''), ''),
        })
    elif request.method == 'POST':
        data = request.form
        action = data.get('action', '')
        if action == 'add_client':
            try:
                if not validate_phone_number(data['clientphone']):
                    return redirect(url_for('portal_handler', m='eph'))
                if not validate_name(data['coachname']):
                    return redirect(url_for('portal_handler', m='enm'))
                if not validate_name(data['clientname']):
                    return redirect(url_for('portal_handler', m='enm'))
                phone = normalize_phone_number(data['clientphone'])
                with context.database.transaction() as trans:
                    coach = trans.get_coaches(data['coachname']).first()
                    if coach is None:
                        return redirect(url_for('portal_handler', m='cnf'))

                    trans.new_client(name=data['clientname'],
                                     phone=phone,
                                     lang=data['clientlang'],
                                     coach_id=coach.id)
            except IntegrityError:
                return redirect(url_for('portal_handler', m='phn'))
            except Exception as e:
                logging.error(str(e))
                return redirect(url_for('portal_handler', m='err'))
            else:
                try:
                    context.sms_gateway.send_sms(data['clientphone'],
                                                 with_lang(data['clientlang'],
                                                           'welcome'))
                except Exception as e:
                    context.logger.error(str(e))
                    return redirect(url_for('portal_handler', m='php'))
                else:
                    return redirect(url_for('portal_handler', m='ok0'))

        elif action == 'add_coach':
            try:
                if not validate_name(data['coachname']):
                    return redirect(url_for('portal_handler', m='enm'))
                url = data['coachurl'].strip()
                if len(url) == 0:
                    return redirect(url_for('portal_handler', m='eur'))
                with context.database.transaction() as trans:
                    trans.new_coach(name=data['coachname'],
                                    email=data['coachemail'],
                                    ics_url=url)
            except IntegrityError:
                return redirect(url_for('portal_handler', m='cdu'))
            except Exception as e:
                logging.error(str(e))
                return redirect(url_for('portal_handler', m='err'))
            else:
                return redirect(url_for('portal_handler', m='ok1'))

        elif action == 'remove_client':
            try:
                if not validate_name(data['clientname']):
                    return redirect(url_for('portal_handler', m='enm'))
                if not validate_phone_number(data['clientphone']):
                    return redirect(url_for('portal_handler', m='eph'))
                phone = normalize_phone_number(data['clientphone'])
                with context.database.transaction() as trans:
                    n = trans.delete_client(name=data['clientname'],
                                            phone_number=phone)
                    if n == 0:
                        return redirect(url_for('portal_handler', m='nrm'))
            except Exception as e:
                logging.error(str(e))
                return redirect(url_for('portal_handler', m='err'))
            else:
                return redirect(url_for('portal_handler', m='ok2'))

        elif action == 'remove_coach':
            try:
                if not validate_name(data['coachname']):
                    return redirect(url_for('portal_handler', m='enm'))
                with context.database.transaction() as trans:
                    coach = trans.get_coaches(name=data['coachname']).first()
                    if coach is None:
                        return redirect(url_for('portal_handler', m='cnf'))

                    clients = list(trans.get_clients()
                                        .filter_by(coach_id=coach.id))
                    if len(clients) > 0:
                        return redirect(url_for('portal_handler', m='chc'))

                    trans.delete(coach)
            except Exception as e:
                logging.error(str(e))
                return redirect(url_for('portal_handler', m='err'))
            else:
                return redirect(url_for('portal_handler', m='ok3'))

        elif action == 'reassign_client':
            try:
                if not validate_name(data['coachname']):
                    return redirect(url_for('portal_handler', m='enm'))
                if not validate_name(data['clientname']):
                    return redirect(url_for('portal_handler', m='enm'))
                with context.database.transaction() as trans:
                    client = trans.get_clients(name=data['clientname']).first()
                    if client is None:
                        return redirect(url_for('portal_handler', m='cmf'))

                    coach = trans.get_coaches(name=data['coachname']).first()
                    if coach is None:
                        return redirect(url_for('portal_handler', m='cnf'))

                    client.coach_id = coach.id
            except Exception as e:
                context.logger.error(str(e))
                return redirect(url_for('portal_handler', m='err'))
            else:
                with context.database.transaction() as trans:
                    client = trans.get_clients(name=data['clientname']).first()
                    if client is None:
                        # Someone else could have deleted the
                        # client in the meantime
                        return redirect(url_for('portal_handler', m='cmf'))
                    try:
                        context.sms_gateway.send_sms(client.phone_number,
                                                     with_lang(client.language,
                                                               'coach_change')
                                                     .format(data['coachname']))
                    except Exception as e:
                        context.logger.error(str(e))
                        return redirect(url_for('portal_handler', m='php'))
                    else:
                        return redirect(url_for('portal_handler', m='ok4'))


def client_info(client_id: int):
    DATETIME_FMT = "%A, %b %d, %Y at %I:%M %p"
    WAITING_STATE = Client.STATE_WAITING_COACH_CONFIRM
    if request.method == 'GET':
        with context.database.transaction() as transaction:
            client = transaction._session.query(Client).get(client_id)
            reschedule_states = {
                    Client.STATE_CLEAN: 'Normal',
                    Client.STATE_RESCHEDULE: 'Just starting rescheduling',
                    Client.STATE_WAITING_COACH_CONFIRM: ('Waiting for coach '
                                                         'confirmation'),
                    Client.STATE_WAITING_CLIENT_CONFIRM: ('In the middle of '
                                                          'rescheduling'),
            }

            appointment_passed = False
            old_time = None
            new_time = None
            needs_coach_confirmation = None

            if client is not None:
                needs_coach_confirmation = client.reschedule_state == \
                                             WAITING_STATE
                if needs_coach_confirmation:
                    [old, new] = map(iso8601.parse_date, client.
                                     reschedule_extra.
                                     split(Context.DATETIME_SEPARATOR))

                    if datetime.now(timezone.utc) > old or \
                            datetime.now(timezone.utc) > new:
                        # The appointment has passed
                        appointment_passed = True
                    else:
                        local_old = old.astimezone(Context.DEFAULT_TIMEZONE)
                        local_new = new.astimezone(Context.DEFAULT_TIMEZONE)
                        old_time = local_old.strftime(DATETIME_FMT)
                        new_time = local_new.strftime(DATETIME_FMT)

            return render_template('client.jinja', **{
                'secret': url_for('portal_handler'),
                'client': client,
                'reschedule_states': reschedule_states,
                'appointment_passed': appointment_passed,
                'needs_coach_confirmation': needs_coach_confirmation,
                'old_time': old_time,
                'new_time': new_time,
                'message': {
                    'br': "That request was invalid",
                    'approved': ("Reschedule approved, "
                                 "client has been notified."),
                    'rejected': ("Reschedule rejected, "
                                 "client has been notified."),
                    'err': "Database error!",
                    'eagain': ("Unable to message the client, please try "
                               "again. Their status has not been updated.")
                }.get(request.args.get('m', ''), '')
            })
    elif request.method == 'POST':
        data = request.form
        try:
            with context.database.transaction() as trans:
                client = trans._session.query(Client).get(client_id)
                if client is None:
                    # already shows client not found message
                    return redirect(url_for('client_info',
                                            client_id=client_id))
                if client.reschedule_state != WAITING_STATE:
                    return redirect(url_for('client_info',
                                            client_id=client_id, m='br'))
                if data['confirm'] == 'approve':
                    try:
                        context.sms_gateway.send_sms(client.phone_number,
                                                     with_lang(client.language,
                                                               'approved'))
                    except Exception:
                        context.logger.warn(traceback.format_exc())
                        return redirect(url_for('client_info',
                                                client_id=client_id,
                                                m='eagain'))
                    else:
                        client.reschedule_state = Client.STATE_CLEAN
                        client.rescedule_extra = ''
                        return redirect(url_for('client_info',
                                                client_id=client_id,
                                                m='approved'))
                elif data['confirm'] == 'reject':
                    try:
                        context.sms_gateway.send_sms(client.phone_number,
                                                     with_lang(client.language,
                                                               'rejected'))
                    except Exception:
                        context.logger.warn(traceback.format_exc())
                        return redirect(url_for('client_info',
                                                client_id=client_id,
                                                m='eagain'))
                    else:
                        client.reschedule_state = Client.STATE_CLEAN
                        client.rescedule_extra = ''
                        return redirect(url_for('client_info',
                                                client_id=client_id,
                                                m='rejected'))
                else:
                    return redirect(url_for('client_info',
                                            client_id=client_id,
                                            m='br'))
        except Exception as e:
            logging.error(str(e))
            return redirect(url_for('client_info',
                                    client_id=client_id, m='err'))


if __name__ == '__main__':
    # Create an app context
    config_path = os.environ.get(CONFIG_ENV, 'config.yml')
    context = Context(config_path)
    app.run(port=8080,
            host=context.config[Context.CONF_KEY_BIND_ADDRESS],
            debug=context.debug)
