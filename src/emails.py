# This file is part of CWCCFG.
#
# CWCCFG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CWCCFG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CWCCFG.  If not, see <http://www.gnu.org/licenses/>.

import abc

from smtplib import SMTP
import yaml


class EmailException(Exception):
    pass


class EmailGateway(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def send_email(self, to_address: str, subject: str, message: str):
        """
        Send an email message.

        to_address is the target recepient of the email.
        subject is the subject of the email
        message is a Unicode (default) string.

        Returns true if successful, false otherwise.
        """
        raise NotImplementedError()


class SmtpEmailGateway(EmailGateway):

    def __init__(self, config: dict):
        """
        Initialize a SmtpEmailGateway object.

        config is a dict with values:

        host: a string, the name of the host running your SMTP server. You
            can specifiy either the IP address of the host or a domain name.
        port: an int, the port where the SMTP server is listening.
        from_address: a string, the address of the email user,
            e.g., johndoe@domainname.org
        from_name: a string, the name of the email user, e.g., John Doe.
        """
        self.host = config['host']
        self.port = int(config['port'])
        self.from_address = config['from_address']
        self.from_name = config['from_name']

    def send_email(self, to_address: str, subject: str, message: str):
        message = '\n'.join(['From: {} <{}>',
                             'To: {}',
                             'Subject: {}',
                             '',
                             '{}']).format(self.from_name,
                                           self.from_address,
                                           to_address,
                                           subject,
                                           message)
        try:
            with SMTP(self.host, self.port) as smtp:
                smtp.sendmail(self.from_address, [to_address], message)
        except Exception as e:
            raise EmailException(e)


class MockEmailGateway(EmailGateway):

    def __init__(self, config: dict):
        """
        Initialize a  MockEmailGateway object.

        config is a dict with values:

        output_file: the relative path to a file. Any data sent
          to this object will be written to this file as YAML.
        """
        self.output_file = open(config['output_file'], 'w')
        self.data = []

    def send_email(self, to_address: str, subject: str, message: str):
        mail = {
            'to_address': to_address,
            'subject': subject,
            'message': message,
        }
        self.data.append(mail)
        self.output_file.write(yaml.dump(mail))


PROVIDERS = {
    'mock': MockEmailGateway,
    'smtp': SmtpEmailGateway
}
