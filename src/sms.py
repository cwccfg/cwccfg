# This file is part of CWCCFG.
#
# CWCCFG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CWCCFG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CWCCFG.  If not, see <http://www.gnu.org/licenses/>.

import abc

from twilio.rest import TwilioRestClient
from flask import Response
import twilio.twiml as twiml

from message import Message


class SMSGateway(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def send_sms(self, number: str, message: str):
        """
        Send an SMS message.
        The number must be a country code followed by a valid 10-digit number.

        The message is a Unicode (default) string.
        """
        raise NotImplementedError()

    @staticmethod
    @abc.abstractmethod
    def parse_message(self, message: dict) -> Message:
        raise NotImplementedError()

    @staticmethod
    @abc.abstractmethod
    def build_response(self) -> Response:
        raise NotImplementedError()


class TwilioSMSGateway(SMSGateway):

    def __init__(self, account_id: str, token: str, outgoing_number: str):
        """
        Create an Twilio SMS Gateway.

        The arguments specify the Twilio account to use:
            id, the account id
            token, the account access token
            outgoing_number, the number which messages will be sent from.
                This number must be prefixed with a "+" and the country code.
        """

        self._internal = TwilioRestClient(account_id, token)
        self.outgoing_number = outgoing_number

    def send_sms(self, number: str, message: str):
        self._internal.messages.create(to=number,
                                       from_=self.outgoing_number,
                                       body=message)

    @staticmethod
    def parse_message(message: dict) -> Message:
        return Message(message['MessageSid'],
                       message['From'],
                       message['To'],
                       message['Body'])

    @staticmethod
    def build_response(message: str) -> Response:
        r = twiml.Response()
        if message is not None:
            r.message(message)
        return Response(str(r), mimetype='text/xml')
