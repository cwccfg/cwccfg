#!/usr/bin/env bash

# This file is part of CWCCFG.
#
# CWCCFG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CWCCFG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CWCCFG.  If not, see <http://www.gnu.org/licenses/>.

set -e
set -u
set -o pipefail

initial_dir=

setup() {
    : ${NO_VENV="false"}  # Set if unset
    if ! [[ "${NO_VENV}" == "true" ]]; then
        cd $HOME # To handle windows
        rm -rf test_venv
        python3 -m venv test_venv

        # Virtualenv activation scripts use unknown variables :(
        set +u
        source test_venv/bin/activate
        set -u

        cd "${initial_dir}"
    fi
    pip3 install -r requirements.txt >/dev/null
}

cleanup() {
    if ! [[ "${NO_VENV}" == "true" ]]; then
        cd $HOME
        rm -rf test_venv
        cd "${initial_dir}"
    fi
}

run_lint_test() {
    flake8 src tests
}

run_license_header_test() {
    find src/ tests/ salt/ lang/ \
        -type f -regextype posix-egrep -regex '.*(py|sh|sls|yml)' \
        -print0 | while IFS= read -r -d '' source_file; do
            tests/license_header.py "${source_file}"
    done
    tests/license_header.py Vagrantfile
    tests/license_header.py test.sh
}

run_twilio_unknown_number_test() {
    rm -f tests/database.sqlite
    tests/twilio_unknown_number.py tests/config_sms_unknown_client.yml
    rm -f tests/database.sqlite
}

run_sorted_requirements_test() {
    LANG=en_US.UTF-8 sort --check --ignore-case --dictionary-order requirements.txt
}

run_tests() {
    initial_dir=$(pwd)

    # Run tests that don't require the venv
    echo "Testing sorted requirements..."
    run_sorted_requirements_test
    echo "Testing license headers..."
    run_license_header_test

    echo "Setting up..."
    setup

    # Run tests that require the venv
    echo "Testing lints..."
    run_lint_test
    echo "Testing twilio unknown numbers..."
    run_twilio_unknown_number_test

    echo "Cleaning up... (almost done!)"
    cleanup
    echo "All tests passed!"
}

run_tests "$@"
