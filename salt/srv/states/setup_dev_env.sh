#!/usr/bin/env bash

# This file is part of CWCCFG.
#
# CWCCFG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CWCCFG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CWCCFG.  If not, see <http://www.gnu.org/licenses/>.

printf "setting up development environment..."

PATH="$(ruby -e 'print Gem.user_dir')/bin:$PATH"

if [[ ! -e "${HOME}/venv" ]]; then
  cd "${HOME}"
  python -m venv venv
fi

cd /vagrant
source "${HOME}/venv/bin/activate"
pip install -r requirements.txt >/dev/null

printf "Done\n"
