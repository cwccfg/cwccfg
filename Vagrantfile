# This file is part of CWCCFG.
#
# CWCCFG is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CWCCFG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with CWCCFG.  If not, see <http://www.gnu.org/licenses/>.

module OS
  def OS.windows?
    (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
  end
end

Vagrant.configure(2) do |config|
  if Vagrant.has_plugin?('vagrant-cachier') and not OS.windows?
    config.cache.scope = :machine
  end

  config.vm.box = "terrywang/archlinux"
  config.vm.hostname = "cwcfg"

  config.vm.provider :virtualbox do |vbox|
     vbox.memory = 1024
     if Gem::Version.new(Vagrant::VERSION) >= Gem::Version.new('1.8.0')
       vbox.linked_clone = true
     end
  end

  config.vm.synced_folder "salt/srv", "/srv/salt"
  config.vm.provision :salt do |salt|
    salt.masterless = true
    salt.minion_config = "salt/minion.yml"
    salt.run_highstate = true
    salt.log_level = 'info'
    salt.verbose = true
    salt.colorize =true
  end

  config.vm.network "forwarded_port", guest: 8080, host: 8080
end
